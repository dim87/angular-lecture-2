import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Blog} from '../entities/blog';

@Component({
    selector: 'app-blog-create',
    templateUrl: './blog-create.component.html',
    styleUrls: ['./blog-create.component.css']
})
export class BlogCreateComponent implements OnInit {

    form: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
            id: new FormControl(''),
            title: new FormControl(''),
            body: new FormControl(''),
            userId: new FormControl(''),
        });
    }

    updateValues(blog: Blog) {
        this.form.patchValue(blog);
    }

    ngOnInit(): void {
        this.form.get('userId').setValue('54321');
    }

    submit() {
        const formValues: Blog = this.form.getRawValue();
        console.log(formValues);

        const userId = this.form.get('userId').value;
        console.log(userId);
    }
}
