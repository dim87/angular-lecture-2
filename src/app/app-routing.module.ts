import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BlogCreateComponent} from './blog-create/blog-create.component';
import {BlogListComponent} from './blog-list/blog-list.component';


const routes: Routes = [
    {
        // localhost:4200/blogs
        path: 'blogs',
        component: BlogListComponent
    },
    {
        // localhost:4200/blogs/create
        path: 'blogs/create',
        component: BlogCreateComponent
    },
    {
        // localhost:4200/blogs/update/3
        path: 'blogs/update/:blogId',
        component: BlogCreateComponent
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
