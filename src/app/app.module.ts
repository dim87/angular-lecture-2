import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BlogListComponent} from './blog-list/blog-list.component';
import {BlogComponent} from './blog/blog.component';
import {BlogCreateComponent} from './blog-create/blog-create.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BlogService} from './blog.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        BlogListComponent,
        BlogComponent,
        BlogCreateComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,

        HttpClientModule,
    ],
    providers: [
        BlogService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
