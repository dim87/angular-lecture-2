import {Component, OnInit, ViewChild} from '@angular/core';
import {Blog} from '../entities/blog';
import {BlogCreateComponent} from '../blog-create/blog-create.component';
import {BlogService} from '../blog.service';

@Component({
    selector: 'app-blog-list',
    templateUrl: './blog-list.component.html',
    styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {

    blogs: Blog[] = [];

    @ViewChild(BlogCreateComponent) blogCreateComponent: BlogCreateComponent;

    constructor(private blogService: BlogService) {
    }

    ngOnInit(): void {
        this.blogService.getAll().subscribe(result => {
            this.blogs = result;
        });
    }

    fillForm(blog: Blog) {
        this.blogCreateComponent.updateValues(blog);
    }

    delete(blog: Blog) {
        this.blogService.delete(blog.id).subscribe(result => {
            this.blogs = this.blogs.filter(item => item.id !== blog.id);
            // this.ngOnInit();
        });
    }
}
