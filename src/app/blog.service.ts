import {Injectable} from '@angular/core';
import {Blog} from './entities/blog';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BlogService {

    constructor(private httpClient: HttpClient) {
    }

    getAll(): Observable<Blog[]> {
        return this.httpClient.get<Blog[]>('https://jsonplaceholder.typicode.com/posts');
    }

    delete(blogId: number): Observable<any> {
        return this.httpClient.delete('https://jsonplaceholder.typicode.com/posts/' + blogId);
    }
}
